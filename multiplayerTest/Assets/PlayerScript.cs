﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Photon.Pun;
using Photon.Realtime;

public class PlayerScript : MonoBehaviourPunCallbacks
{

    public static GameObject LocalPlayer;
    // Start is called before the first frame update
    void Start()
    {
        LocalPlayer = this.gameObject;

        DontDestroyOnLoad(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {

        if (photonView.IsMine)
        {
            transform.Translate(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        }
        
    }
}
