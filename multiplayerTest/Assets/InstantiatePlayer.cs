﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Photon.Pun;
using Photon.Realtime;

public class InstantiatePlayer : MonoBehaviourPunCallbacks
{
    // Start is called before the first frame update
    void Start()
    {

        if (PlayerScript.LocalPlayer == null)
        {
            PhotonNetwork.Instantiate("Player", new Vector3(0, 1f, 0), Quaternion.identity);
        }

   
    }

    // Update is called once per frame
    
}
